/**
 * ThePayMail API
 * This API offers an interface to ThePayMail services  Build information: commit-id: 2af2ebca9f8813183a7ed261446b742086cc3828 commit-time: 2020-09-14T20:41:02+0000 branch: develop 
 *
 * OpenAPI spec version: 1.0.0
 * Contact: info@thepaymail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ContactData } from './contactData';


export interface UserRequest { 
    contactData?: ContactData;
    givenName: string;
    password: string;
    position?: string;
    role: UserRequest.RoleEnum;
    surname: string;
    title: UserRequest.TitleEnum;
}
export namespace UserRequest {
    export type RoleEnum = 'PROCESS_OWNER' | 'SUPPORT' | 'SERVICE' | 'SERVICE_ASSISTANT' | 'BILLING' | 'CONTROLLER' | 'PARTS' | 'TECH' | 'ADMIN';
    export const RoleEnum = {
        PROCESSOWNER: 'PROCESS_OWNER' as RoleEnum,
        SUPPORT: 'SUPPORT' as RoleEnum,
        SERVICE: 'SERVICE' as RoleEnum,
        SERVICEASSISTANT: 'SERVICE_ASSISTANT' as RoleEnum,
        BILLING: 'BILLING' as RoleEnum,
        CONTROLLER: 'CONTROLLER' as RoleEnum,
        PARTS: 'PARTS' as RoleEnum,
        TECH: 'TECH' as RoleEnum,
        ADMIN: 'ADMIN' as RoleEnum
    };
    export type TitleEnum = 'MR' | 'MS' | 'DIVERS';
    export const TitleEnum = {
        MR: 'MR' as TitleEnum,
        MS: 'MS' as TitleEnum,
        DIVERS: 'DIVERS' as TitleEnum
    };
}
