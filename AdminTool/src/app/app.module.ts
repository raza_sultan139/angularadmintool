import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BASE_PATH } from './typescript-angular-client';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { TenantsDetailsComponent } from './components/tenants-details/tenants-details.component';
import { ApiModule} from './typescript-angular-client';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TenantListComponent } from './components/tenants-details/tenant-list/tenant-list.component';
import { TenantCompleteInfoComponent } from './components/tenants-details/tenant-complete-info/tenant-complete-info.component';
import { AddNewTenantComponent } from './components/tenants-details/add-new-tenant/add-new-tenant.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { PaymentConfigurationComponent } from './components/payment-configuration/payment-configuration.component';
import { CountdownModule } from 'ngx-countdown';
import { PaymentServiceProviderComponent } from './components/payment-service-provider/payment-service-provider.component';

// export function apiConfigFactory (): Configuration  {
//   const params: ConfigurationParameters = {
//     // set configuration parameters here.
//     accessToken: ''
    
    
//   }
//   return new Configuration(params);
// }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavBarComponent,
    TenantsDetailsComponent,
    TenantListComponent,
    TenantCompleteInfoComponent,
    AddNewTenantComponent,
    UserManagementComponent,
    PaymentConfigurationComponent,
    PaymentServiceProviderComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CountdownModule,
    ApiModule, 
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['https://api.paymail-dev.de/pm'],
        sendAccessToken: true
      }
    })
  ],
  providers: [{provide: BASE_PATH, useValue: environment.API_BASE_PATH}],
  bootstrap: [AppComponent]
})
export class AppModule { }
