import { Component, OnInit } from '@angular/core';
import { PaymentConfigurationRequest } from 'src/app/typescript-angular-client';
import {TenantAdministrationService} from 'src/app/typescript-angular-client/api/tenantAdministration.service'

@Component({
  selector: 'app-payment-configuration',
  templateUrl: './payment-configuration.component.html',
  styleUrls: ['./payment-configuration.component.css']
})
export class PaymentConfigurationComponent implements OnInit {

  constructor(private paymentConfigService : TenantAdministrationService) { }

  request : PaymentConfigurationRequest = {};
  tenantCode = '0000030';
  active: boolean;
  credential: string;
  paymentMethod: PaymentConfigurationRequest.PaymentMethodEnum;
  paymentServiceProvider: PaymentConfigurationRequest.PaymentServiceProviderEnum;
  position: number;
  principal: string;

  

  ngOnInit(): void {
    
  }

  addPaymentConfig(){
    this.request = {
      active : this.active ,
      credential: this.credential,
      paymentMethod:  this.paymentMethod,
      paymentServiceProvider: this.paymentServiceProvider,
      position: this.position,
      principal: this.principal
      }
    this.paymentConfigService.addPaymentConfigurationUsingPOST(this.request, this.tenantCode).subscribe(data =>{
      console.log('Payment Configuration updated');
      
    })

  }

}
