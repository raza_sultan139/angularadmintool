import { Component, OnInit } from '@angular/core'
import { TenantAdministrationService } from 'src/app/typescript-angular-client/api/tenantAdministration.service';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-new-tenant',
  templateUrl: './add-new-tenant.component.html',
  styleUrls: ['./add-new-tenant.component.css']
})
export class AddNewTenantComponent implements OnInit {

  tenantCode: string = '';
  errorMessage;
  successMessage;

  constructor(private newTenant: TenantAdministrationService) { }

  ngOnInit(): void {

  }



  addNewTenant() {
    this.errorMessage = "";
    //this.successMessage = ""
    this.newTenant.initiateTenantDatabaseUsingPOST(this.tenantCode).subscribe((response) => {
    
      console.log('response received', response)
    }
    , (error) => {                              //Error callback
      console.error('error caught in component')
      this.errorMessage = error;

      
      throw error;
      // if(error instanceof HttpErrorResponse){
      //   if (error.error instanceof ErrorEvent) {
      //     console.error("Error Event");
      //   }else{
      //     console.log(`error status : ${error.status} ${error.statusText}`);
      //     switch (error.status){
      //       case 200 : 
      //       this.successMessage = 'Tenant database is Initialized';
      //       break;
      //     }
      //   }
      // }else {
      //   throw error;
        
      // }

    }
    
    )
  }
}