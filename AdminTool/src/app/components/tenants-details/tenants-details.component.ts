import { Component, OnInit  } from '@angular/core';
import { TenantInfoResponse, Address, ContactData, CommunicationSettings, CurrencyPreferenceEntry,  RepairOrderSettings, DmsSettings, TenantInfoRequest } from 'src/app/typescript-angular-client';
import {TenantAdministrationService} from 'src/app/typescript-angular-client/api/tenantAdministration.service'


@Component({
  selector: 'app-tenants-details',
  templateUrl: './tenants-details.component.html',
  styleUrls: ['./tenants-details.component.css']
})
export class TenantsDetailsComponent implements OnInit   {

  constructor( private getTenantInfo : TenantAdministrationService) { }
  public tenantDataFromListComp : TenantInfoResponse = {};
  tenantCode :string = '';
   tenantAddress : Address = {};
   tenantContactData : ContactData = {};
   communicationSettings : CommunicationSettings = {};
   currencyPreferences: Array<CurrencyPreferenceEntry> = [{
    "currencyUnit": "GBP",
    "preferredCurrency": false
  },
  {
    "currencyUnit": "EUR",
    "preferredCurrency": false
  },
  {
    "currencyUnit": "USD",
    "preferredCurrency": false
  },
  {
    "currencyUnit": "MXN",
    "preferredCurrency": false
  },
  {
    "currencyUnit": "CHF",
    "preferredCurrency": false
  },

];
   repairOrderSettings : RepairOrderSettings = {};
   dmsSettings : DmsSettings= {dmsAuthPassword: '', dmsType: null, dmsAuthUserName: ''};
   request: TenantInfoRequest = {};

   
  ngOnInit(): void {

  }

  showTenantInfoFunction($event){
    this.tenantCode= this.tenantDataFromListComp.tenantCode;
    this.tenantDataFromListComp = $event;
    this.tenantAddress = this.tenantDataFromListComp.address
    this.tenantContactData = this.tenantDataFromListComp.contactData
    this.communicationSettings = this.tenantDataFromListComp.communicationSettings
    this.currencyPreferences = this.tenantDataFromListComp.currencyPreferences
    
    this.repairOrderSettings = this.tenantDataFromListComp.repairOrderSettings
    this.dmsSettings = this.tenantDataFromListComp.dmsSettings

    console.log(this.currencyPreferences);
    console.log(this.tenantDataFromListComp);
    
    
  }
  edit(){
    this.request = {address: this.tenantAddress,
                    baseUrl: this.tenantDataFromListComp.baseUrl,
                  communicationSettings: this.communicationSettings,
                  contactData: this.tenantContactData,
                  currencyPreferences: this.currencyPreferences,
                  defaultLocale: this.tenantDataFromListComp.defaultLocale,
                  defaultTimezone: this.tenantDataFromListComp.defaultTimezone,
                  director: this.tenantDataFromListComp.director,
                  displayName: this.tenantDataFromListComp.displayName,
                  dmsSettings: this.dmsSettings,
                  forwardEmailAddress: this.tenantDataFromListComp.forwardEmailAddress,
                  fullName: this.tenantDataFromListComp.fullName,
                  legalEntityName: this.tenantDataFromListComp.legalEntityName,
                  paymentLinkValidity: this.tenantDataFromListComp.paymentLinkValidity,
                  recipientDataLearning: this.tenantDataFromListComp.recipientDataLearning,
                  registeredSeat: this.tenantDataFromListComp.registeredSeat,
                  registration: this.tenantDataFromListComp.registration,
                  reminderInterval: this.tenantDataFromListComp.reminderInterval,
                  repairOrderSettings: this.repairOrderSettings,
                  returnEmailAddress: this.tenantDataFromListComp.returnEmailAddress,
                  style: this.tenantDataFromListComp.style,
                  surveyScript: this.tenantDataFromListComp.surveyScript,
                  tenantCodeExternal: this.tenantDataFromListComp.tenantCodeExternal,
                  vatCode: this.tenantDataFromListComp.vatCode
                }
    this.tenantCode = this.tenantDataFromListComp.tenantCode
     this.getTenantInfo.updateTenantInfoUsingPUT(this.request, this.tenantCode).subscribe((data: TenantInfoRequest) => {
       console.log('Tenant Info updated');
       
     })

  }
  // fetchSelectedPreferredCurrency() {


  //   // this.preferredCurrency = this.currencyPreferences.pu((value, index) => {
  //   //   return value.preferredCurrency
  //   // });
  // }
  // fetchCheckedCurrencyUnits() {
  //   this.CurrencyUnit = []
  //   this.currencyPreferences.forEach((value, index) => {
  //     if (value.preferredCurrency) {
  //       this.CurrencyUnit.push(value.currencyUnit);
  //     }
  //   });
  //}


}
