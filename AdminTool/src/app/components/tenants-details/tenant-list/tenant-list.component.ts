import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DisplayNameToTenantMappingResponse} from '../../../typescript-angular-client/model/displayNameToTenantMappingResponse';
import {TenantAdministrationService} from 'src/app/typescript-angular-client/api/tenantAdministration.service';
import { TenantInfoResponse, Address } from 'src/app/typescript-angular-client';


@Component({
  selector: 'app-tenant-list',
  templateUrl: './tenant-list.component.html',
  styleUrls: ['./tenant-list.component.css']
})

export class TenantListComponent implements OnInit {
  @Output() toParentEvent = new EventEmitter();
  public tenantsData: any[];
  public tenantIndividualData: any;
  public address : Address = {};
  public mapaddress: any;
  constructor(private tenantsNameAndCode : TenantAdministrationService ) { }

  ngOnInit(): void {
    this.tenantsNameAndCode.getAllTenantsUsingGET().subscribe((data:DisplayNameToTenantMappingResponse[]) =>{
      //console.log(data);
      
      this.tenantsData = data.map(name =>[name.tenantCode]+' '.concat(name.displayName));
     // console.log(this.tenantsData);


    this.tenantsData  = ['-- Select Tenant --'].concat(this.tenantsData);
    }),error => {
      console.error(error);
    }
    
  

  }
  onChangeTenant = (event) => {
    let selectedValue = event.target.value;
    
    if(selectedValue !== '-- Select Tenant --'){
    let selectedTenantCode = selectedValue.split(" ", 1);
     //Get Tenant Info with service
    this.tenantsNameAndCode.getTenantInfoUsingGET(selectedTenantCode).subscribe((data : TenantInfoResponse) => { 
      this.tenantIndividualData = data;
      this.sendDataToParentComponent();
     
   this.address = data.address
  
  //  console.log(this.tenantIndividualData);
   
  // console.log(this.address);

  
  
      
      })
    }
  }
  sendDataToParentComponent(){
        this.toParentEvent.emit(this.tenantIndividualData)
  }

}
