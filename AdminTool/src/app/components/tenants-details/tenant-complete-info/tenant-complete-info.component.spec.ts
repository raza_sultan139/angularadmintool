import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantCompleteInfoComponent } from './tenant-complete-info.component';

describe('TenantCompleteInfoComponent', () => {
  let component: TenantCompleteInfoComponent;
  let fixture: ComponentFixture<TenantCompleteInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenantCompleteInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantCompleteInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
