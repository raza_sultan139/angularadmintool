import { Component, OnInit } from '@angular/core';
import { OAuthService, OAuthErrorEvent } from 'angular-oauth2-oidc';
import {authPasswordFlowConfig } from '../../services/OauthService';
import {AuthService} from '../../services/auth.service'
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 public userName: string;
  password: string;
  loginFailed: boolean = false;
  userProfile: object;
  headers : HttpHeaders; 
  

  constructor(private oauthService: OAuthService) { 
    this.oauthService.events.subscribe(e => e instanceof OAuthErrorEvent ? console.error(e) : console.warn(e));
     // Tweak config for password flow
    this.oauthService.configure(authPasswordFlowConfig)
   // this.oauthService.refreshToken().then(info => console.debug('refresh ok', info));
     if (!oauthService.hasValidAccessToken()){
        this.oauthService.silentRefresh()
       .then(info => console.debug('refresh ok', info)).catch(err => console.error('refresh error', err))
     }
  

  }

  ngOnInit(): void {
  }
  
  get access_token() {
    //  if(!this.oauthService.hasValidAccessToken()){
    //   return this.oauthService.silentRefresh()
    // }else
  return  this.oauthService.getAccessToken();
  }

  get access_token_expiration() {
  
    return this.oauthService.getAccessTokenExpiration();
  }
        //***Custom made oauth service if decided not to use OIDC library ***//

  // loginWithPassword() {
  //   this.oauthService.login(this.userName,this.password).subscribe(() =>{
  //     console.log('authenticated');
      
  //   })
  // }

  loginWithPassword() {
   this.headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' ,
  'Authorization' : 'Basic Y2xpZW50OnNlY3JldA==' });
    this.oauthService
      .fetchTokenUsingPasswordFlow(
        this.userName,
        this.password, this.headers
      )
      .then(() => {
        console.debug('successfully logged in');
        this.loginFailed = false;
      })
      .catch(err => {
        console.error('error logging in', err);
        this.loginFailed = true;
      });
  }
  

  logout() {
    this.oauthService.logOut(true);
  }

}
