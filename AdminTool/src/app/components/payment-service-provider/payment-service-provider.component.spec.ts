import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentServiceProviderComponent } from './payment-service-provider.component';

describe('PaymentServiceProviderComponent', () => {
  let component: PaymentServiceProviderComponent;
  let fixture: ComponentFixture<PaymentServiceProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentServiceProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentServiceProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
