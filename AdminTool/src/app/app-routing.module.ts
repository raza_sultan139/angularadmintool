import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { TenantsDetailsComponent } from './components/tenants-details/tenants-details.component';
import { AddNewTenantComponent } from './components/tenants-details/add-new-tenant/add-new-tenant.component';
import {AuthGuard} from 'src/app/services/auth.guard'
import { UserManagementComponent } from './components/user-management/user-management.component';
import { PaymentConfigurationComponent } from './components/payment-configuration/payment-configuration.component';
import { PaymentServiceProviderComponent} from './components/payment-service-provider/payment-service-provider.component';
const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "login", component: LoginComponent},
  {path: "tenants", component : TenantsDetailsComponent},
  {path: "initiateTenantDatabase", component : AddNewTenantComponent},
  {path: "userManagement" , component: UserManagementComponent},
  {path: "paymentConfiguration" , component: PaymentConfigurationComponent},
  {path : "addPSP", component: PaymentServiceProviderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
