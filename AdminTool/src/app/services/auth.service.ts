// This is custom made oauth service if decided not to use OIDC library
import { Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url: string = 'https://api.paymail-dev.de/pm/oauth/token';
  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    
    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' ,
     'Authorization' : 'Basic Y2xpZW50OnNlY3JldA==' });

    let body = new URLSearchParams();
    body.set('grant_type', "password");
    body.set('username', username);
    body.set('password', password);
   //body.set('client_id', "client");
    //body.set('client_secret', "secret");
    //body.set('scope', "read write");
    
    return this.http.post<any>(this.url, body.toString(),{ headers: headers } )
    .pipe(
      map(jwt => {
        if (jwt && jwt.access_token) {
          localStorage.setItem('token', JSON.stringify(jwt))
        }
      })
    );
}
}
